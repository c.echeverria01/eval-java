package com.example.evaluacionjavabci.controllers;

import com.example.evaluacionjavabci.entities.UserRequestDTO;
import com.example.evaluacionjavabci.entities.UserResponseDTO;
import com.example.evaluacionjavabci.services.UserService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.Optional;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService service;
    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable UUID id){

        Optional<UserResponseDTO> userResponseDTOOptional = Optional.ofNullable(service.findById(id));

        if(userResponseDTOOptional.isPresent()){
            return ResponseEntity.ok(userResponseDTOOptional.orElseThrow());
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping("/sign-up")
    public UserResponseDTO save(@RequestBody UserRequestDTO user){
        return service.save(user);
    }


}
