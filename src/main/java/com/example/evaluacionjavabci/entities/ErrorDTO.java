package com.example.evaluacionjavabci.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.ArrayList;

@Data
@Builder
public class ErrorDTO {

    @JsonFormat(pattern = "MMM d, yyyy hh:mm:ss a", locale = "es-ES")
    private LocalDateTime timestamp;
    private Integer codigo;
    private String detail;

}



