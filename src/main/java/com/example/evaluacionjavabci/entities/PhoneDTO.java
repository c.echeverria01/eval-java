package com.example.evaluacionjavabci.entities;

import lombok.Data;

@Data
public class PhoneDTO {
    private Integer number;
    private Integer citycode;
    private String contrycode;
}
