package com.example.evaluacionjavabci.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.ArrayList;
@Data
public class UserResponseDTO {

    private String id;
    @JsonFormat(pattern = "MMM d, yyyy hh:mm:ss a", locale = "es-ES")
    private LocalDateTime created;
    private String lastLogin;
    private String token;
    private boolean isActive;
    private String name;
    private String email;
    private String password;
    private ArrayList<PhoneDTO> phones;
}
