package com.example.evaluacionjavabci.entities;

import jakarta.persistence.OneToMany;
import lombok.Data;

import java.util.List;
@Data
public class UserRequestDTO {
    private String name;
    private String email;
    private String password;
    private List<PhoneDTO> phones;
}
