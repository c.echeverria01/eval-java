package com.example.evaluacionjavabci.repositories;

import com.example.evaluacionjavabci.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;
@Repository
public interface UserRepository extends CrudRepository<User, UUID> {
}
