package com.example.evaluacionjavabci.services;

import com.example.evaluacionjavabci.entities.*;
import com.example.evaluacionjavabci.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.modelmapper.ModelMapper;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService{


    private final ModelMapper modelMapper;

    @Autowired
    private UserRepository repository;
    @Override
    @Transactional(readOnly = true)
    public List<User> findAll() {
        return (List<User>) repository.findAll();
    }

    @Override
    @Transactional()
    public UserResponseDTO save(UserRequestDTO userDto) {
        User user = this.modelMapper.map(userDto, User.class);

     /*   List<Phone> phones =new ArrayList<>();
        for(PhoneDTO phoneDTO:userDto.getPhones()){
            Phone phone=this.modelMapper.map(phoneDTO, Phone.class);
            phone.setUser(user);
            user.getPhones().add(phone);
        }
        user.setPhones(phones); */

        user.setIsActive(true);
        user.setCreated(LocalDateTime.now());

        User savedUser = repository.save(user);

        return modelMapper.map(savedUser, UserResponseDTO.class);
    }

    @Override
    @Transactional(readOnly = true)
    public UserResponseDTO findById(UUID id) {

        Optional<User> optionalUser = repository.findById(id);
        return this.modelMapper.map( optionalUser.get(), UserResponseDTO.class);
    }
}
