package com.example.evaluacionjavabci.services;

import com.example.evaluacionjavabci.entities.User;
import com.example.evaluacionjavabci.entities.UserRequestDTO;
import com.example.evaluacionjavabci.entities.UserResponseDTO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

public interface UserService {

    List<User> findAll();

    UserResponseDTO save(UserRequestDTO user);

    UserResponseDTO findById(UUID id);

}
