package com.example.evaluacionjavabci.exceptions;

import com.example.evaluacionjavabci.entities.ErrorDTO;
import org.springframework.boot.context.config.ConfigDataResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.naming.AuthenticationException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.time.LocalDateTime;

@RestControllerAdvice
public class HandlerExceptions {

    @ExceptionHandler(value = RuntimeException.class)
    public ResponseEntity<ErrorDTO> handleUserNotFoundException(RuntimeException e) {
        ErrorDTO errorResponse = ErrorDTO.builder().codigo(404).timestamp(LocalDateTime.now()).detail(e.getMessage()).build();
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
    }

    @ExceptionHandler(AuthenticationException.class)
    public ResponseEntity<Object> handleAuthenticationException(AuthenticationException e) {
        ErrorDTO errorResponse = ErrorDTO.builder().codigo(403).timestamp(LocalDateTime.now()).detail(e.getMessage()).build();
        return ResponseEntity.status(HttpStatus.FORBIDDEN)
                .body(errorResponse);
    }


    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleException(Exception e) {
        ErrorDTO errorResponse = ErrorDTO.builder().codigo(500).timestamp(LocalDateTime.now()).detail(e.getMessage()).build();

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(errorResponse);
    }

    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    public ResponseEntity<Object> handleSQLIntegrityConstraintViolationException(SQLIntegrityConstraintViolationException e) {
        ErrorDTO errorResponse = ErrorDTO.builder().codigo(400).timestamp(LocalDateTime.now()).detail(e.getMessage()).build();
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(errorResponse);
    }


}
