INSERT INTO users (name, email, created, last_login, token, is_active, password)
VALUES ('John Doe', 'john.doe@example.com', '2023-01-01 12:00:00', '2023-01-01 12:05:00', 'f6f86f4d-9eae-4d64-97be-6d818547a641', true, 'pass123');

INSERT INTO users (name, email, created, last_login, token, is_active, password)
VALUES ('Jane Smith', 'jane.smith@example.com', '2023-02-15 10:30:00', '2023-02-15 10:35:00', 'a1b2c3d4-e5f6-7890-1234-56789abcde01', true, 'securePwd456');

-- Insertar teléfonos
INSERT INTO phones (citycode, contrycode, number, user_id) VALUES (123456789, '123', 123, (SELECT id FROM users WHERE email = 'john.doe@example.com'));

INSERT INTO phones (citycode, contrycode, number, user_id) VALUES (123456789, '123', 123, (SELECT id FROM users WHERE email = 'jane.smith@example.com'));

SELECT* FROM users;